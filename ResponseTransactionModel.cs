﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{
    public class ResponseTransactionModel
    {
        public string CardId { get; set; }
        public string AttractionId { get; set; }
        public decimal AttractionPrice { get; set; }
        public decimal Balance { get; set; }
        public string ResponseType { get; set; }


        public string GenerateResponse()
        {
            //string sumAmountText = "";
            //string attractionPriceText = "";

            //if (Balance.ToString().Length < 6)
            //{
            //    for (int i = Balance.ToString().Length; i < 6; i++)
            //    {
            //        sumAmountText += "0";
            //    }
            //    sumAmountText += Balance.ToString();
            //}
            //else
            //{
            //    sumAmountText = Balance.ToString();
            //} 
            return CardId + "," + AttractionId + "," + AttractionPrice.ToString("00.00", CultureInfo.InvariantCulture) + "," + Balance.ToString("000.00", CultureInfo.InvariantCulture) + "," + ResponseType;
        }
    }
}
