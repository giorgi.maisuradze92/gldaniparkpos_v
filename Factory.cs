﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{     
    public class Factory
    {
        private Dictionary<string, string> sendCodeList;
        static bool _continue;
        static SerialPort _serialPort;
        private static SerialPort _serialPortRegistration;

        private List<AttractionPriceModel> _attractionList;

        public Factory(List<AttractionPriceModel> list)
        {
            sendCodeList = new Dictionary<string, string>();
            _attractionList = list;
            foreach (var attractionPriceModel in list)
            {
                sendCodeList.Add(attractionPriceModel.AttractionId, $"000000,{attractionPriceModel.AttractionId},{attractionPriceModel.AttractionPrice},000.00,0");
            }
        }

        public Factory()
        {

        }
        public bool CheckRegistrator(string com)
        {
            bool response = false;
            _serialPortRegistration = new SerialPort
            {
                PortName = com,
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            };

            try
            {
                if (!_serialPortRegistration.IsOpen)
                {
                    _serialPortRegistration.Open();
                }


                var send = "STATUS";
                _serialPortRegistration.WriteLine(send);
                Thread.Sleep(50);
                var x = _serialPortRegistration.ReadExisting();

                if (x == "1")
                {
                    response = true;
                }
                _serialPortRegistration.Close();
            }
            catch (Exception ex)
            {
                _serialPortRegistration.Close();
                response = false;
            }
            return response;
        }


        public bool TestAttractionSystem(string com)
        {
            bool response = false;

            _serialPort = new SerialPort
            {
                PortName = com,
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            };

            try
            {
                if (!_serialPort.IsOpen)
                {
                    _serialPort.Open();
                }

                for (int i = 0; i < _attractionList.Count; i++)
                {
                    Thread.Sleep(40);

                    var send = sendCodeList[_attractionList[i].AttractionId];

                    _serialPort.WriteLine(send);
                    var x = _serialPort.ReadExisting();
                    var array = x.Split(',');

                    if (array[0] == "000000" && array.LastOrDefault() == "0" && array.Length == 5)
                    {
                        return true;
                    }
                }

                _serialPort.Close();
                response = false;
            }
            catch (Exception ex)
            {
                _serialPort.Close();
                response = false;
            }
            return response;
        }


        public void RunAttraction(string com)
        {
            _serialPort?.Close();

            using (_serialPort = new SerialPort
            {
                PortName = com,
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            })
            {

                string cardId;
                decimal amount;
                decimal attractionPrice;
                string attractionId;

                if (!_serialPort.IsOpen)
                {
                    _serialPort.Open();
                }
                while (true)
                {


                    // ReSharper disable once ForCanBeConvertedToForeach
                    for (int i = 0; i < _attractionList.Count; i++)
                    {
                        Thread.Sleep(40);
                        try
                        {
                            var send = sendCodeList[_attractionList[i].AttractionId];

                            _serialPort.WriteLine(send);
                            sendCodeList[_attractionList[i].AttractionId] =
                                $"000000,{_attractionList[i].AttractionId},{_attractionList[i].AttractionPrice},000.00,0";
                            var x = _serialPort.ReadExisting();
                            var array = x.Split(',');
                           // Console.WriteLine(x);
                            if (array[0] != "000000" && array.LastOrDefault() == "1" && array.Length == 5)
                            {
                                cardId = array[0];
                                attractionId = array[1];
                                attractionPrice = decimal.Parse(_attractionList
                                    .FirstOrDefault(y => y.AttractionId == attractionId).AttractionPrice, CultureInfo.InvariantCulture);

                                amount = Convert.ToDecimal(array[3].Split('.')[0] + "," + array[3].Split('.')[1]);
                                new Thread(delegate ()
                                {
                                    CalculateResponse(cardId, amount, attractionPrice, attractionId);
                                    
                                }).Start();

                            }
                        }
                        catch
                        {
                        }
                        if (i + 1 == _attractionList.Count)
                        {
                            Thread.Sleep(50);
                        }
                    }

                }
            }
        }


        public void RunRegistrator(string com)
        {
            _serialPortRegistration?.Close();

            using (_serialPortRegistration = new SerialPort
            {
                PortName = com,
                BaudRate = 115200,
                Parity = Parity.None,
                DataBits = 8,
                StopBits = StopBits.One,
                Handshake = Handshake.None,
                ReadTimeout = 500,
                WriteTimeout = 500
            })
            {

                _serialPortRegistration.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);

                if (!_serialPortRegistration.IsOpen)
                {
                    _serialPortRegistration.Open();
                }


                while (true)
                {
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private static void DataReceivedHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            string indata = sp.ReadExisting();
            var array = indata.Split(',');
            string cardId = array[0].Substring(1, array[0].Length - 1);

            bool exist = CheckCard(cardId);

            if ((array.LastOrDefault() == "1*" || array.LastOrDefault() == "3*") && exist)
            {
                if (array.LastOrDefault() == "1*")
                {
                    var balance = GetCardAmount(cardId);
                    string send = $"{array[0]},00,00.00,{balance.ToString("000.00", CultureInfo.InvariantCulture)},2*";

                    sp.WriteLine(send);
                }

                if (array.LastOrDefault() == "3*")
                {
                    var balance = AddBalanceToCard(cardId, decimal.Parse(array[3], CultureInfo.InvariantCulture));
                    //aq dajameba unda baratis 
                    string send = $"{array[0]},00,00.00,{balance.ToString("000.00", CultureInfo.InvariantCulture)},2*";
                    Console.WriteLine($"CardId = {cardId}, Add Amount = {decimal.Parse(array[3], CultureInfo.InvariantCulture)}, New Amount = {balance.ToString("000.00", CultureInfo.InvariantCulture)} ");
                    sp.WriteLine(send);
                }
            }
            else if (array.LastOrDefault() == "5*" && !exist && array.Length == 5)
            {
                // რა ნომერიც მოვა იმ ნომერს ვიღებთ და ვამოწმებთ თუ არის ბაზაში და თუ არაა 
                //ვუგზავით და თუ არა ვუწერთ ერორს
                bool error = CheckCard(cardId);
                var send = !error ? $"{array[0]},00,00.00,000.00,6*" : $"{array[0]},00,00.00,ERROR,2*";
                sp.WriteLine(send);
            } else if (array.LastOrDefault() == "7*" && !exist && array.Length == 5)
            {
                 SaveCard(cardId);
            } else
            {
                string send = $"{array[0]},00,00.00,ERROR ,2*";
                sp.WriteLine(send);
            }
        }


        public void CalculateResponse(string cardId, decimal amount, decimal attractionPrice, string attractionId)
        {
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spCheckTransactionAndSave", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@CardId", cardId);
                cmd.Parameters.AddWithValue("@AttractionPrice", attractionPrice);
                cmd.Parameters.AddWithValue("@AttractionId", attractionId);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        ResponseTransactionModel response = new ResponseTransactionModel();
                        response.CardId = dataReader["CardId"].ToString();
                        response.AttractionId = dataReader["AttractionId"].ToString();
                        response.AttractionPrice = Convert.ToDecimal(dataReader["AttractionPrice"]);
                        response.Balance = Convert.ToDecimal(dataReader["Balance"]);
                        response.ResponseType = dataReader["ResponseType"].ToString();

                        sendCodeList[attractionId] = response.GenerateResponse();
                        if(response.ResponseType == "1")
                            Console.WriteLine($"CardId = {cardId}, Amount = {attractionPrice}, attractionId = {attractionId}, Balanse = {response.Balance}");
                        else
                            Console.WriteLine($"No Enough Money, CardId = {cardId}, attractionId = {attractionId}, Balanse = {response.Balance}");   
                    }

                    dataReader.Close();
                }

                con.Close();
            }
        }


        public static decimal GetCardAmount(string cardId)
        {
            decimal balance = 0;
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spGetCardBalance", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@CardId", cardId);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        balance = Convert.ToDecimal(dataReader["Balance"]);
                    }
                    dataReader.Close();
                }

                con.Close();
            }

            return balance;
        }


        public static decimal AddBalanceToCard(string cardId, decimal amount)
        {
            decimal balance = 0;
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spAddBalanceToCard", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@CardId", cardId);
                cmd.Parameters.AddWithValue("@Balance", amount);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        balance = Convert.ToDecimal(dataReader["Balance"]);
                    }
                    dataReader.Close();
                }
                con.Close();
            }

            return balance;
        }


        public static void SaveCard(string cardId)
        {
           
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spSaveCard", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@CardId", cardId);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    
                    dataReader.Close();
                }
                con.Close();
            } 
        }


        public static bool CheckCard(string cardId)
        {
            bool exist = false;
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spIsCardExist", con))
            {
                con.Open();
                cmd.Parameters.AddWithValue("@CardId", cardId);
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        exist = Convert.ToBoolean(dataReader["Exist"]);
                    }
                    dataReader.Close();
                }
                con.Close();
            }
            return exist;
        }

    }
}
