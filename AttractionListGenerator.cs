﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{
    public class AttractionListGenerator
    {
        public List<AttractionPriceModel> GetAttractionPriceList()
        {
            List<AttractionPriceModel> attractionPriceModels = new List<AttractionPriceModel>();
            string connectionString = Properties.Settings.Default.ConnectionString;
            using (SqlConnection con = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand("spGetAttractionPriceList", con))
            {
                con.Open(); 
                cmd.CommandType = CommandType.StoredProcedure;

                using (SqlDataReader dataReader = cmd.ExecuteReader())
                {
                    while (dataReader.Read())
                    {
                        AttractionPriceModel response = new AttractionPriceModel();
                        response.AttractionId = dataReader["AttractionId"].ToString();
                        response.AttractionPrice = Convert.ToDecimal(dataReader["Price"]).ToString("00.00", CultureInfo.InvariantCulture);
                        attractionPriceModels.Add(response);
                    }

                    dataReader.Close();
                }

                con.Close();
            }

            return attractionPriceModels;
        }
    }




}
 
