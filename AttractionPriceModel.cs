﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{
    public class AttractionPriceModel
    {
        public string AttractionId { get; set; }
        public string AttractionPrice { get; set; }
    }
}
