﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{
    public class AttractionModel
    {
        public string AttractionId { get; set; }
        public string AttractionName { get; set; }
    }
}
