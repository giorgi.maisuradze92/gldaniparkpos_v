﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestGldaniParkPos
{
    public class SerialPortGetter
    {
        public List<string> SerialPorts()
        {
            List<string> serialPorts = new List<string>();
            foreach (string s in SerialPort.GetPortNames())
            {
                serialPorts.Add(s);
            }

            return serialPorts;
        }
    }
}
